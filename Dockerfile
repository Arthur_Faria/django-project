FROM python:3.8-buster

WORKDIR /src

COPY /src .

EXPOSE 8000/tcp

RUN pip3 install Django

CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]