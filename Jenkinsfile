pipeline {
    agent any
    environment {
        PATH = "/var/lib/jenkins/.local/bin:${env.PATH}"
    }
    stages {
        stage('Valida python, pip e Dockerfile') {
            steps {
                sh 'python3 -V'
                sh 'pip3 -V'
                script {
                    if(!fileExists('./Dockerfile')){
                        error('Dockerfile não encontrado')
                    }
                    echo 'Dockerfile encontrado'
                }
            }
        }
        stage('Instalando requirements') {
            steps {
                sh 'pip3 install -r requirements.txt --no-cache-dir --disable-pip-version-check'
            }
        }
        stage('Validação do código') {
            steps {
                sh 'find . -name \\*.py | xargs pylint -f parseable | tee pylint.log'
                sh 'find . -name \\*.py | xargs pycodestyle | tee pycodestyle.log'
            }
            post {
                always {
                    recordIssues (
                        tool: pyLint(pattern: '**/pylint.log'),
                        unstableTotalAll: 50,
                        failedTotalAll: 100,
                    )
                    recordIssues (
                        tool: pep8(pattern: '**/pycodestyle.log'),
                        unstableTotalAll: 50,
                        failedTotalAll: 100,
                    )
                }
            }
        }
        // stage('Executa teste unitário') {
        //     steps{
        //         sh 'find . -name "*.py" | xargs pytest --junitxml=pytest.xml'
        //     }
        //     post {
        //         always {
        //             junit '**/pytest.xml'
        //         }
        //     }
        // }
        stage('Build docker image') {
            steps {
                script {
                    img = docker.build("arthurresendefaria/django-projeto:${env.BUILD_NUMBER}")
                }
            }
        }

        stage('Push imagem docker') {
            steps {
                script {
                    docker.withRegistry('https://registry.hub.docker.com/', 'dockerhub-login') {
                        img.push('latest')
                        img.push("${env.BUILD_NUMBER}")
                    }
                }
            }
        }
    }
    post {
        success {
            echo "processo número ${env.BUILD_NUMVER} finalizado com sucesso"
        }
        failure {
            echo "processo número ${env.BUILD_NUMVER} finalizado com falha"
        }
        unstable {
            echo "processo número ${env.BUILD_NUMVER} finalizado demarcado como instável"
        }
        always {
            echo "processo número ${env.BUILD_NUMVER} finalizado"
        }
    }
}
